class FoodmenusController < ApplicationController
  before_action :set_foodmenu, only: [:show, :edit, :update, :destroy]

  # GET /foodmenus
  # GET /foodmenus.json
  def index
    @foodmenus = Foodmenu.all
  end

  # GET /foodmenus/1
  # GET /foodmenus/1.json
  def show
  end

  # GET /foodmenus/new
  def new
    @foodmenu = Foodmenu.new
  end

  # GET /foodmenus/1/edit
  def edit
  end

  # POST /foodmenus
  # POST /foodmenus.json
  def create
    @foodmenu = Foodmenu.new(foodmenu_params)

    respond_to do |format|
      if @foodmenu.save
        format.html { redirect_to @foodmenu, notice: 'Foodmenu was successfully created.' }
        format.json { render :show, status: :created, location: @foodmenu }
      else
        format.html { render :new }
        format.json { render json: @foodmenu.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /foodmenus/1
  # PATCH/PUT /foodmenus/1.json
  def update
    respond_to do |format|
      if @foodmenu.update(foodmenu_params)
        format.html { redirect_to @foodmenu, notice: 'Foodmenu was successfully updated.' }
        format.json { render :show, status: :ok, location: @foodmenu }
      else
        format.html { render :edit }
        format.json { render json: @foodmenu.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /foodmenus/1
  # DELETE /foodmenus/1.json
  def destroy
    @foodmenu.destroy
    respond_to do |format|
      format.html { redirect_to foodmenus_url, notice: 'Foodmenu was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_foodmenu
      @foodmenu = Foodmenu.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def foodmenu_params
      params.require(:foodmenu).permit(:name, :price, :restaurent_id)
    end
end
