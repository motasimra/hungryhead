json.array!(@foodmenus) do |foodmenu|
  json.extract! foodmenu, :id, :name, :price, :restaurent_id
  json.url foodmenu_url(foodmenu, format: :json)
end
