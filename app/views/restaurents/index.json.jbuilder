json.array!(@restaurents) do |restaurent|
  json.extract! restaurent, :id, :name, :location
  json.url restaurent_url(restaurent, format: :json)
end
