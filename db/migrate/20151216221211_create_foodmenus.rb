class CreateFoodmenus < ActiveRecord::Migration
  def change
    create_table :foodmenus do |t|
      t.string :name
      t.integer :price

      t.timestamps null: false
    end
  end
end
