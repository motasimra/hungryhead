require 'test_helper'

class FoodmenusControllerTest < ActionController::TestCase
  setup do
    @foodmenu = foodmenus(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:foodmenus)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create foodmenu" do
    assert_difference('Foodmenu.count') do
      post :create, foodmenu: { name: @foodmenu.name, price: @foodmenu.price }
    end

    assert_redirected_to foodmenu_path(assigns(:foodmenu))
  end

  test "should show foodmenu" do
    get :show, id: @foodmenu
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @foodmenu
    assert_response :success
  end

  test "should update foodmenu" do
    patch :update, id: @foodmenu, foodmenu: { name: @foodmenu.name, price: @foodmenu.price }
    assert_redirected_to foodmenu_path(assigns(:foodmenu))
  end

  test "should destroy foodmenu" do
    assert_difference('Foodmenu.count', -1) do
      delete :destroy, id: @foodmenu
    end

    assert_redirected_to foodmenus_path
  end
end
